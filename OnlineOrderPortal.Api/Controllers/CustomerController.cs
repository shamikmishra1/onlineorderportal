﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OnlineOrderPortal.Data;
using OnlineOrderPortal.Data.Models;

namespace OnlineOrderPortal.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private OnlineOrderPortalDataContext dataContext;

        public CustomerController(OnlineOrderPortalDataContext dataContext)
        {
            this.dataContext = dataContext;
        }        
        
        [HttpPost("CreateCustomer")]
        public IActionResult CreateCustomer(Customer customer)
        {
            if (this.dataContext.Customers.Any(o => o.Email == customer.Email))
                return this.BadRequest("Cannot create new customer. A Customer already exists with this email");
            
            this.dataContext.Customers.Add(new Customer
            {
                Name = customer.Name,
                Email = customer.Email
            });
            dataContext.SaveChanges();
            return this.Ok("New customer registered");
        }

        [HttpGet("GetAllCustomers")]
        public List<Customer> GetAllCustomers()
        {
            var resultAllCustomersList = this.dataContext.Customers.Select(o => o).ToList();
            return resultAllCustomersList;
        }

        [HttpGet("GetCustomersWithoutOrder")]
        public IActionResult GetCustomersWithoutOrder()
        {
            var resultAllCustomerList = this.dataContext.Customers.Include(o => o.Orders).Where(o => o.Orders != null).ToList();
            if (resultAllCustomerList != null && resultAllCustomerList.Count != 0)
                return this.Ok(resultAllCustomerList);
            return this.Ok("No customers found");
        }

        [HttpGet("GetCustomersWithOrder")]
        public IActionResult GetCustomersWithOrder([FromBody]string name)
        {
            if (string.IsNullOrEmpty(name))
                return this.BadRequest("Please give customer name");
            var customerId = this.dataContext.Customers.Single(o => o.Name == name).Id;
            var resultOrderList = this.dataContext.Orders.Where(o => o.CustomerId == customerId);
            return this.Ok(resultOrderList);
        }
    }
}