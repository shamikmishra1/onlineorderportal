﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OnlineOrderPortal.Data;
using OnlineOrderPortal.Data.Models;

namespace OnlineOrderPortal.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private OnlineOrderPortalDataContext dataContext;

        public OrderController(OnlineOrderPortalDataContext dataContext)
        {
            this.dataContext = dataContext;
        }

        [HttpPost("CreateOrder")]
        public IActionResult CreateOrder([FromBody]Order order)
        {
             this.dataContext.Orders.Add(new Order
            {
                Price = order.Price,
                CreatedDate = order.CreatedDate,
                CustomerId = order.CustomerId
            });
            dataContext.SaveChanges();
            var resultOrderList = this.dataContext.Orders.Where(o => o.CustomerId == order.CustomerId).ToList();
            return this.Ok(resultOrderList);
        }

        [HttpGet("GetCustomerOrder")]
        public IActionResult GetCustomerOrder([FromQuery]string customerName)
        {
            var customerId = this.dataContext.Customers.SingleOrDefault(o => o.Name == customerName).Id;
            var resultOrderList = this.dataContext.Orders.Where(o => o.CustomerId == customerId).ToList();
            return this.Ok(resultOrderList);
        }
    }
}