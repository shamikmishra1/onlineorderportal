﻿using Newtonsoft.Json;
using OnlineOrderPortal.Controllers;
using OnlineOrderPortal.Data.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace OnlineOrderPortal.Tests
{
    public class CustomerTest
    {
        [Fact]
        public void GetCustomersWithoutOrderTest()
        {
           
        }

        [Fact]
        public void GetCustomersWithOrderTest()
        {
            var client = new RestClient("https://localhost:44304");
            var request = new RestRequest("api/GetCustomersWithoutOrder", Method.GET);
            var response = client.Execute(request);
            var json = response.Content;
            var resultList = JsonConvert.DeserializeObject<List<Customer>>(response.Content);
            Assert.NotNull(resultList);
        }
    }
}
