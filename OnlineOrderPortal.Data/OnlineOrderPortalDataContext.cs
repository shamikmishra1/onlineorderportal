﻿using Microsoft.EntityFrameworkCore;
using OnlineOrderPortal.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineOrderPortal.Data
{
    public class OnlineOrderPortalDataContext : DbContext
    {
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Order> Orders { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(@"Data Source = LAPTOP-F34T66FV\SQLEXPRESS; Initial Catalog = LAPTOP-F34T66FV\SQLEXPRESS; Persist Security Info = True; User ID = Database-Dev; Password = database123; ", options =>
                {
                    options.UseRowNumberForPaging();
                });        
            }
        }
       
        public OnlineOrderPortalDataContext(DbContextOptions<OnlineOrderPortalDataContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.HasDefaultSchema("dbo"); 
        }
    }
}
