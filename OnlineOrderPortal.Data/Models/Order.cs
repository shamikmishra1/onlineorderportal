﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OnlineOrderPortal.Data.Models
{
    [Table("OnlineOrderPortal.Order")]
    public class Order
    {
        public int Id { get; set; }
        public double Price { get; set; }
        public DateTime CreatedDate { get; set; }

        public int CustomerId { get; set; }
    }
}

