﻿using Microsoft.EntityFrameworkCore;
using OnlineOrderPortal.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OnlineOrderPortal.Data.Repositories
{
    public class SqlRepository
    {
        private OnlineOrderPortalDataContext dataContext;
        public SqlRepository(OnlineOrderPortalDataContext dataContext)
        {
            this.dataContext = dataContext;
        }

        public List<Customer> GetAll()
        {
            return this.dataContext.Customers.Select(o => o).ToList();
        }

        public List<Customer> GetCustomersWithoutOrder()
        {
            var resultAllCustomerList = this.dataContext.Customers.Include(o => o.Orders).Where(o => o.Orders != null).ToList();
            return resultAllCustomerList;
        }

        public void CreateCustomer(Customer customer)
        {
            this.dataContext.Customers.Add(new Customer
            {
                Name = customer.Name,
                Email = customer.Email
            });
            dataContext.SaveChanges();
        }
    }
}
