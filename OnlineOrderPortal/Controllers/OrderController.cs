﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using OnlineOrderPortal.Data;
using OnlineOrderPortal.Data.Models;
using RestSharp;

namespace OnlineOrderPortal.Controllers
{
    public class OrderController : Controller
    {
        private OnlineOrderPortalDataContext dataContext;

        public OrderController(OnlineOrderPortalDataContext dataContext)
        {
            this.dataContext = dataContext;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult GetCustomerOrder(string customerName)
        {
            var client = new RestClient("https://localhost:44350");
            var request = new RestRequest("api/order/GetCustomerOrder?customerName="+customerName, Method.GET);
            
            var response = client.Execute(request);
            var json = response.Content;
            var resultOrderList = JsonConvert.DeserializeObject<List<Order>>(response.Content);
            return this.PartialView("OrderResultPartial", resultOrderList);
        }

        public IActionResult AddOrder()
        {
            return this.View("AddOrder");
        }

        [HttpPost]
        public IActionResult CreateOrder(string customerName, int price)
        {
            var client = new RestClient("https://localhost:44350");
            var request = new RestRequest("api/order/CreateOrder", Method.POST);
            request.AddJsonBody(new
            {
                Price = price,
                CreatedDate = DateTime.Now,
                customerId = this.dataContext.Customers.SingleOrDefault(o => o.Name == customerName).Id
            });
            var response = client.Execute(request);
            var json = response.Content;
            var resultOrderList = JsonConvert.DeserializeObject<List<Order>>(response.Content);
            return this.PartialView("OrderResultPartial", resultOrderList);

        }

    }
}