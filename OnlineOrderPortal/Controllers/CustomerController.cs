﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using OnlineOrderPortal.Data.Models;
using OnlineOrderPortal.Models;
using RestSharp;

namespace OnlineOrderPortal.Controllers
{
    public class CustomerController : Controller
    {
        //  public IActionResult Index()
        // {
        //    return View();
        // }

        public IActionResult New()
        {
            return this.View(new Customer());
        }

        [HttpPost]
        public IActionResult Create(Customer customer)
        {
            var client = new RestClient("https://localhost:44350");
            var request = new RestRequest("api/customer/CreateCustomer", Method.POST);
            request.AddJsonBody(new
            {
                Name = customer.Name,
                Email = customer.Email
            });
            var response = client.Execute(request);
            return this.Redirect("New");
        }

        [HttpGet]
        public IActionResult GetCustomersWithoutOrders()
        {
            var resultCustomersList = new List<Customer>();

            var client = new RestClient("https://localhost:44350");
            var request = new RestRequest("api/customer/GetCustomersWithoutOrder", Method.GET);
            
            var response = client.Execute(request);
            var json =  response.Content;
            resultCustomersList = JsonConvert.DeserializeObject<List<Customer>>(response.Content);
            return this.View("Index", resultCustomersList);
        }
        
        
    }
}